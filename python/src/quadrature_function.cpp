#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <fenics-solid-mechanics/QuadratureFunction.h>
#include <fenics-solid-mechanics/StateUpdate.h>

namespace py = pybind11;
namespace fsm = fenicssolid;

PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fenicssolid_wrappers
{
  void quadrature_function(py::module& m)
  {
    py::class_<fsm::QuadratureFunction, std::shared_ptr<fsm::QuadratureFunction>, dolfin::GenericFunction>
      (m, "QuadratureFunction", "Class to ease the handling of integration point values", py::multiple_inheritance())
      .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
           std::shared_ptr<const dolfin::FiniteElement>,
           const std::vector<double>&>(), "Create a QuadratureFunction")
      .def(py::init<std::shared_ptr<const dolfin::Mesh>,
           std::shared_ptr<const dolfin::FiniteElement>,
           std::shared_ptr<fsm::StateUpdate>,
           const std::vector<double>&>(), "Create a QuadratureFunction")
      .def("value_rank", &fsm::QuadratureFunction::value_rank)
      .def("value_dimension", &fsm::QuadratureFunction::value_dimension)
//      .def("value_shape", &fsm::QuadratureFunction::value_shape)
      .def("restrict", &fsm::QuadratureFunction::restrict)
      .def("compute_vertex_values", &fsm::QuadratureFunction::compute_vertex_values)
//      .def("compute_mean", &fsm::QuadratureFunction::compute_mean)
      .def("element", &fsm::QuadratureFunction::element);
//      .def("id", &fsm::QuadratureFunction::id);
  }
}
