
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/log/log.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <fenics-solid-mechanics/HistoryData.h>

namespace py = pybind11;
namespace fsm = fenicssolid;

namespace fenicssolid_wrappers
{
  void history_data(py::module& m)
  {
    py::class_<fsm::HistoryData>
      (m, "HistoryData", "Class to ease the handling of history-dependent values")
      .def(py::init<std::shared_ptr<const dolfin::Mesh>,
           std::shared_ptr<const dolfin::FiniteElement>,
           const std::size_t>(), "Create an HistoryData instance")
//       FIXME MISSING WRAPPERS 
       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 3, 3>>) //6,6
       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 3, 3>>) //6,6
//       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 6, 1>>)
//       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 6, 1>>)
//       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 1, 1>>)
//       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 1, 1>>)
      .def("update_history", &fsm::HistoryData::update_history)
      .def("compute_mean", &fsm::HistoryData::compute_mean)
      .def("old_data", &fsm::HistoryData::old_data)
      .def("hash_old", &fsm::HistoryData::hash_old)
      .def("hash_current", &fsm::HistoryData::hash_current);
  }
}
